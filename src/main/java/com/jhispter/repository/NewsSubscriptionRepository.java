package com.jhispter.repository;

import com.jhispter.domain.NewsSubscription;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the NewsSubscription entity.
 */
@SuppressWarnings("unused")
public interface NewsSubscriptionRepository extends JpaRepository<NewsSubscription,Long> {

}
